1. Membuat database
MariaDB [(none)]> create database myshop;

2. Membuat tabel
MariaDB[]>use myshop;
MariaDB [myshop]> CREATE TABLE users(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

MariaDB [myshop]> CREATE TABLE categories(
    -> id int primary key auto_increment,
    -> name varchar(255)
    -> );

MariaDB [myshop]> CREATE TABLE items(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> foreign key(category_id) references categories(id)
    -> );

3. Memasukkan data pada tabel
MariaDB [myshop]> insert into users(name,email,password) values(
    -> "John Doe","john@doe.com","john123"),
    -> ("Jane Doe","jane@doe.com","jenita123");

MariaDB [myshop]> insert into categories(name) values
    -> ("gadget"),
    -> ("cloth"),
    -> ("men"),
    -> ("women"),
    -> ("branded");

MariaDB [myshop]> insert into items(name,description,price,stock,category_id) values
    -> ("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),
    -> ("Uniklooh","baju keren dari brand ternama",500000,50,2),
    -> ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4. Mengambil data dari database
a. Data users
MariaDB [myshop]> select id, name, email from users;

b. Data items
MariaDB [myshop]> select * from items where price > 1000000;
MariaDB [myshop]> select * from items where name like "%uniklo%";

c. Data categories
MariaDB [myshop]> select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;

5. Mengubah data tabel
MariaDB [myshop]> update items set price = 2500000 where name = "Sumsang b50"; 
